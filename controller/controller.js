const titleEl = document.getElementById("title");
const descriptionEl = document.getElementById("description");

const takeFormInput = function () {
  const title = titleEl.value;
  const description = descriptionEl.value;

  const checkTitle = checkEmpty(title, "tit-err", `This field can't be empty`);

  const checkDescription = checkEmpty(
    description,
    "des-err",
    `This field can't be empty`
  );

  if (checkTitle && checkDescription) {
    return {
      title,
      description,
    };
  }
};

const renderList = function (dataArr) {
  let contentHTML = "";

  dataArr.forEach((todo) => {
    contentHTML += `
      <tr class="todos-item">
        <td>${todo.id}</td>
        <td>${todo.title}</td>
        <td>${todo.description}</td>
        <td>
        <input onChange='marked(${todo.id})' type='checkbox' ${
      todo.isCompleted ? "checked" : ""
    }/>
        <button onclick='modifyBtn(${
          todo.id
        })'><i class="fa fa-edit"></i></button>
        <button onclick='deleteBtn(${
          todo.id
        })'><i class="fa fa-trash"></i></button>
        </td>
      </tr>
    `;
  });
  document.getElementById("todos-list").innerHTML = contentHTML;
};

const loaderOn = function () {
  document.getElementById("loader").style.display = "flex";
};

const loaderOff = function () {
  document.getElementById("loader").style.display = "none";
};

const updateBtnOn = function () {
  document.getElementById("btn-update").style.display = "inline-block";
};
const updateBtnOff = function () {
  document.getElementById("btn-update").style.display = "none";
};

const addBtnOn = function () {
  document.getElementById("btn-add").style.display = "inline-block";
};

const addBtnOff = function () {
  document.getElementById("btn-add").style.display = "none";
};

const formReset = function () {
  document.getElementById("title").value = "";
  document.getElementById("description").value = "";
};

const showMessage = function (idErr, message) {
  document.getElementById(idErr).innerHTML = message;
};

const hideMessage = function (idErr) {
  document.getElementById(idErr).innerHTML = "";
};

const resetMessage = function () {
  document.getElementById("tit-err").innerHTML = "";
  document.getElementById("des-err").innerHTML = "";
};
