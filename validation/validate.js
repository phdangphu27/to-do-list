const checkEmpty = function (input, idErr, message) {
  if (!input) {
    showMessage(idErr, message);
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};
