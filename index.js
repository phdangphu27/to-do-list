const BASE_URL = "https://63666d7ef5f549f052c5e6c8.mockapi.io";

const getAllData = function () {
  return axios({
    url: `${BASE_URL}/TODOLIST-API`,
    method: "GET",
  });
};

const resetSelection = function () {
  // document.getElementById("all").setAttribute("selected", true);
};

let idModified = null;

updateBtnOff();

const fetchAllData = function () {
  loaderOn();

  getAllData()
    .then((res) => {
      loaderOff();
      renderList(res.data);
    })
    .catch((err) => {
      console.error(err);
      loaderOff();
    });
};
fetchAllData();

const addBtn = function () {
  const formInputData = takeFormInput();
  if (formInputData) {
    loaderOn();

    const newTodo = {
      title: formInputData.title,
      description: formInputData.description,
      isCompleted: false,
    };

    axios({
      url: `${BASE_URL}/TODOLIST-API`,
      method: "POST",
      data: newTodo,
    })
      .then((res) => {
        formReset();
        fetchAllData();
        sortAll();
        resetSelection();
      })
      .catch((err) => {
        console.error(err);
        loaderOff();
      });
  } else {
    return;
  }
};

const modifyBtn = function (id) {
  updateBtnOn();
  addBtnOff();

  axios({
    url: `${BASE_URL}/TODOLIST-API/${id}`,
    method: "GET",
  })
    .then((res) => {
      document.getElementById("title").value = res.data.title;
      document.getElementById("description").value = res.data.description;

      idModified = res.data.id;
    })
    .catch((err) => {
      console.error(err);
      loaderOff();
    });
};

const updateBtn = function () {
  loaderOn();
  const formInputData = takeFormInput();

  axios({
    url: `${BASE_URL}/TODOLIST-API/${idModified}`,
    method: "PUT",
    data: formInputData,
  })
    .then((res) => {
      formReset();
      resetSelection();
      fetchAllData();
      updateBtnOff();
      addBtnOn();
    })
    .catch((err) => {
      console.error(err);
      loaderOff();
    });
};

const deleteBtn = function (id) {
  loaderOn();
  axios({
    url: `${BASE_URL}/TODOLIST-API/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchAllData();
    })
    .catch((err) => {
      console.error(err);
      loaderOff();
    });
};

const marked = function (id) {
  loaderOn();
  let todoObj = null;

  axios({
    url: `${BASE_URL}/TODOLIST-API/${id}`,
    method: "GET",
  })
    .then((res) => {
      resetSelection();
      todoObj = res.data;
      todoObj.isCompleted = !todoObj.isCompleted;
      updateMarked(todoObj);
    })
    .catch((err) => {
      console.log(err);
      loaderOff();
    });
};

const updateMarked = function (todoObj) {
  axios({
    url: `${BASE_URL}/TODOLIST-API/${todoObj.id}`,
    method: "PUT",
    data: todoObj,
  })
    .then((res) => {
      resetMessage();
      fetchAllData();
    })
    .catch((err) => {
      console.error(err);
      loaderOff();
    });
};

const sortCompleted = function () {
  getAllData()
    .then((res) => {
      const newArr = res.data.filter((todo) => {
        return todo.isCompleted === true;
      });
      renderList(newArr);
    })
    .catch((err) => {
      console.error(err);
    });
};

const sortAll = function () {
  getAllData()
    .then((res) => {
      renderList(res.data);
    })
    .catch((err) => {
      console.error(err);
    });
};

const sortUncompleted = function () {
  getAllData()
    .then((res) => {
      const newArr = res.data.filter((todo) => {
        return todo.isCompleted === false;
      });
      renderList(newArr);
    })
    .catch((err) => {
      console.error(err);
    });
};

const sort = function () {
  const selection = document.getElementById("progress").value;
  if (selection === "all") sortAll();

  if (selection === "completed") sortCompleted();

  if (selection === "uncompleted") sortUncompleted();
};
